package Pract4_RopaUML;
import java.time.LocalDate;

public class Lote {
	
	public Lote() {
		super();
		}
	
	public Lote(int numLote, int numPiezas, LocalDate Fecha) {
		super();
		this.NumLote = numLote;
		NumPiezas = numPiezas;
		this.Fecha= Fecha;
	}
	private int NumLote;
	private int NumPiezas;
	private LocalDate Fecha;
	public float CostoProduccion(float costoXUnidad) {
		return costoXUnidad*this.NumPiezas;
	}
	public float MontoRecuperacionPorLote(float costoXUnidad) {
		return this.CostoProduccion(costoXUnidad)*0.05f;
	}
	public float MontoRecuperacionPorPieza(float costoXUnidad) {
		return costoXUnidad*0.15f;
	}
	@Override
	public String toString() {
		return "Lote [NumLote=" + NumLote + ", NumPiezas=" + NumPiezas + ", Fecha=" + Fecha + "]";
	}
	public int getNumLote() {
		return NumLote;
	}
	public void setNumLote(int numLote) {
		NumLote = numLote;
	}
	public int getNumPiezas() {
		return NumPiezas;
	}
	public void setNumPiezas(int numPiezas) {
		NumPiezas = numPiezas;
	}
	public LocalDate getFecha() {
		return Fecha;
	}
	public void setFecha(LocalDate fecha) {
		Fecha = fecha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Fecha == null) ? 0 : Fecha.hashCode());
		result = prime * result + NumLote;
		result = prime * result + NumPiezas;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lote other = (Lote) obj;
		if (Fecha == null) {
			if (other.Fecha != null)
				return false;
		} else if (!Fecha.equals(other.Fecha))
			return false;
		if (NumLote != other.NumLote)
			return false;
		if (NumPiezas != other.NumPiezas)
			return false;
		return true;
	}
	public int compareTo(Lote arg0) {
			int r = 0;
			if (this.NumLote != arg0.getNumLote())
				return this.NumLote > arg0.getNumLote() ? 1 : -1;
			else if (this.NumPiezas != arg0.getNumPiezas())
				return this.NumPiezas > arg0.getNumPiezas() ? 2 : -2;
			else if (!this.Fecha.equals(arg0.getFecha()))
				return this.Fecha.compareTo(arg0.getFecha());
			return r;
	}
}